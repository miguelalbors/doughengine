/*
  DOUGH ENGINE
  Copyright (C) 2020 Yawin <tuzmakel@gmail.com>

  This software is provided 'as-is', without any express or implied
  warranty.  In no event will the authors be held liable for any damages
  arising from the use of this software.

  Permission is granted to anyone to use this software for any purpose,
  including commercial applications, and to alter it and redistribute it
  freely, subject to the following restrictions:

  1. The origin of this software must not be misrepresented; you must not
     claim that you wrote the original software. If you use this software
     in a product, an acknowledgment in the product documentation would be
     appreciated but is not required.
  2. Altered source versions must be plainly marked as such, and must not be
     misrepresented as being the original software.
  3. This notice may not be removed or altered from any source distribution.
*/

#ifndef CAMERA_H
#define CAMERA_H

#include <RosquilleraReforged/rf_process.h>
#include <RosquilleraReforged/rf_assetmanager.h>
#include "sprite.h"

class CameraFollower : public RF_Process
{
  public:
    static CameraFollower *instance;
    CameraFollower();
    virtual ~CameraFollower();

    virtual void Update();
    virtual void LateDraw();
    void setTarget(string t, bool smooth);
    string getTarget();
    inline float& Speed(){return speed;}

    RF_Structs::Vector2<float> realPosition;

  private:
    string target;
    Sprite* camera = nullptr;
    float speed = 5.0;
};

class Camera : public RF_Process
{
  public:
    static Camera* instance;

    Camera();
    virtual ~Camera();
    virtual void Draw();

    static void Set(string _id, bool smooth = false);
    static string Get();

    template<typename T>
    static void RoomSize(T x, T y)
    {
      instance->roomSize.x = (float) x;
      instance->roomSize.y = (float) y;
    }

    static RF_Structs::Vector2<float> RoomSize();
    static inline float& Speed(){return instance->camera->Speed();}

    RF_Structs::Vector2<float> windowLimit;
    RF_Structs::Vector2<float> roomSize;

  private:
    CameraFollower *camera;
};

#endif //CAMERA_H
