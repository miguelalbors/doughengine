/*
  DOUGH ENGINE
  Copyright (C) 2020 Yawin <tuzmakel@gmail.com>

  This software is provided 'as-is', without any express or implied
  warranty.  In no event will the authors be held liable for any damages
  arising from the use of this software.

  Permission is granted to anyone to use this software for any purpose,
  including commercial applications, and to alter it and redistribute it
  freely, subject to the following restrictions:

  1. The origin of this software must not be misrepresented; you must not
     claim that you wrote the original software. If you use this software
     in a product, an acknowledgment in the product documentation would be
     appreciated but is not required.
  2. Altered source versions must be plainly marked as such, and must not be
     misrepresented as being the original software.
  3. This notice may not be removed or altered from any source distribution.
*/

#ifndef STATEMACHINE_H
#define STATEMACHINE_H

#include <vector>
#include <string>
#include <map>
#include <functional>
using namespace std;

struct State
{
    string id;
    function<void()> action;
    string next;

    State():id(""){}
    State(string _id, function<void()> func, string n = ""):id(_id), action(func), next(n){}
};

class StateMachine
{
  protected:
    void addState(string id, function<void()> func, string next = "");
    void doAction();
    void setState(string state);
    string getState();

  private:
    map<string, State> stateList;
    string activeState = "";
};

#endif //STATEMACHINE_H
