#include "interfaces/statemachine.h"

void StateMachine::addState(string id, function<void()> func, string next)
{
  if(activeState == ""){activeState = id;}
  stateList.emplace(id, State{id, func, next});
}

void StateMachine::doAction()
{
  if(stateList[activeState].action != nullptr)
  {
    stateList[activeState].action();

    if(stateList[activeState].next != "")
    {
      activeState = stateList[activeState].next;
    }
  }
}

void StateMachine::setState(string state)
{
  if(stateList.count(state) != 0)
  {
    activeState = state;
  }
}

string StateMachine::getState()
{
  return activeState;
}
