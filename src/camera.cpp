/*
  DOUGH ENGINE
  Copyright (C) 2020 Yawin <tuzmakel@gmail.com>

  This software is provided 'as-is', without any express or implied
  warranty.  In no event will the authors be held liable for any damages
  arising from the use of this software.

  Permission is granted to anyone to use this software for any purpose,
  including commercial applications, and to alter it and redistribute it
  freely, subject to the following restrictions:

  1. The origin of this software must not be misrepresented; you must not
     claim that you wrote the original software. If you use this software
     in a product, an acknowledgment in the product documentation would be
     appreciated but is not required.
  2. Altered source versions must be plainly marked as such, and must not be
     misrepresented as being the original software.
  3. This notice may not be removed or altered from any source distribution.
*/

#include "camera.h"

#include <RosquilleraReforged/rf_engine.h>
using namespace RF_Structs;

Camera* Camera::instance = nullptr;

Camera::Camera():RF_Process("Camera")
{
  if(instance == nullptr)
  {
    instance = this;
  }
  else
  {
    delete this;
    return;
  }

  camera = RF_Engine::getTask<CameraFollower>(RF_Engine::newTask<CameraFollower>(id));

  windowLimit.x = RF_Engine::MainWindow()->width() >> 1;
  windowLimit.y = RF_Engine::MainWindow()->height() >> 1;
}

Camera::~Camera()
{
  instance = nullptr;
}

void Camera::Set(string _id, bool smooth)
{
  instance->camera->setTarget(_id, smooth);
}

string Camera::Get()
{
  return instance->camera->getTarget();
}

Vector2<float> Camera::RoomSize()
{
  return instance->roomSize;
}

void Camera::Draw()
{
  if(camera != nullptr)
  {
    transform.position.x = -(camera->realPosition.x - (RF_Engine::MainWindow()->width()>>1));
    transform.position.y = -(camera->realPosition.y - (RF_Engine::MainWindow()->height()>>1));
  }
}

CameraFollower* CameraFollower::instance = nullptr;

CameraFollower::CameraFollower():RF_Process("CameraFollower")
{
  if(instance == nullptr)
  {
    instance = this;
  }
  else
  {
    delete this;
    return;
  }
}

CameraFollower::~CameraFollower()
{
  instance = nullptr;
}

void CameraFollower::Update()
{
  if(camera != nullptr)
  {
    float interpolation = speed * RF_Engine::instance->Clock.deltaTime;
    realPosition.x = LERP(realPosition.x, (camera->realPosition.x + camera->offset.x), interpolation);
    realPosition.y = LERP(realPosition.y, (camera->realPosition.y + camera->offset.y), interpolation);

    if(Camera::instance->roomSize.x > 0)
    {
      if(realPosition.x < Camera::instance->windowLimit.x)
      {
        realPosition.x = Camera::instance->windowLimit.x;
      }
      else if(realPosition.x > (Camera::instance->roomSize.x - Camera::instance->windowLimit.x))
      {
        realPosition.x = (Camera::instance->roomSize.x - Camera::instance->windowLimit.x);
      }
    }

    if(Camera::instance->roomSize.y > 0)
    {
      if(realPosition.y < Camera::instance->windowLimit.y)
      {
        realPosition.y = Camera::instance->windowLimit.y;
      }
      else if(realPosition.y > (Camera::instance->roomSize.y - Camera::instance->windowLimit.y))
      {
        realPosition.y = (Camera::instance->roomSize.y - Camera::instance->windowLimit.y);
      }
    }
  }
}
void CameraFollower::LateDraw()
{
  if(Camera::instance != nullptr)
  {
    transform.position = realPosition + Camera::instance->transform.position;
  }
}

void CameraFollower::setTarget(string t, bool smooth)
{
  target = t;
  camera = RF_Engine::getTask<Sprite>(target);
  if(!smooth)
  {
    realPosition = camera->realPosition + camera->offset;
  }
}

string CameraFollower::getTarget()
{
  return target;
}
