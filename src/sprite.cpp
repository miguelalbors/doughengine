/*
  DOUGH ENGINE
  Copyright (C) 2020 Yawin <tuzmakel@gmail.com>

  This software is provided 'as-is', without any express or implied
  warranty.  In no event will the authors be held liable for any damages
  arising from the use of this software.

  Permission is granted to anyone to use this software for any purpose,
  including commercial applications, and to alter it and redistribute it
  freely, subject to the following restrictions:

  1. The origin of this software must not be misrepresented; you must not
     claim that you wrote the original software. If you use this software
     in a product, an acknowledgment in the product documentation would be
     appreciated but is not required.
  2. Altered source versions must be plainly marked as such, and must not be
     misrepresented as being the original software.
  3. This notice may not be removed or altered from any source distribution.
*/

#include "sprite.h"
#include "camera.h"
using namespace RF_Structs;

Sprite::Sprite(string name):RF_Process(name)
{
  animator = new Animator(&graph);
}

Sprite::~Sprite()
{
  delete animator;
}

void Sprite::Draw()
{
  animator->Draw();

  if(graph != nullptr)
  {
    offset.x = -(graph->w >> 1);
    offset.y = -(graph->h);
    if(updateDepth){ zLayer = realPosition.y;}
  }  

  OnSpriteDraw();
}

void Sprite::LateDraw()
{
  if(Camera::instance != nullptr)
  {
    transform.position = realPosition + Camera::instance->transform.position + offset;//Vector2<float>(realPosition.x + Camera::instance->transform.position.x + offset.x, realPosition.y + Camera::instance->transform.position.y + offset.y);
  }

  OnSpriteLateDraw();
}
