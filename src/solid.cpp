/*
  DOUGH ENGINE
  Copyright (C) 2020 Yawin <tuzmakel@gmail.com>

  This software is provided 'as-is', without any express or implied
  warranty.  In no event will the authors be held liable for any damages
  arising from the use of this software.

  Permission is granted to anyone to use this software for any purpose,
  including commercial applications, and to alter it and redistribute it
  freely, subject to the following restrictions:

  1. The origin of this software must not be misrepresented; you must not
     claim that you wrote the original software. If you use this software
     in a product, an acknowledgment in the product documentation would be
     appreciated but is not required.
  2. Altered source versions must be plainly marked as such, and must not be
     misrepresented as being the original software.
  3. This notice may not be removed or altered from any source distribution.
*/

#include "solid.h"
#include <RosquilleraReforged/rf_collision.h>
#include <RosquilleraReforged/rf_assetmanager.h>
using namespace RF_Structs;

bool viewCollisionBox = false;

BoxCollider::BoxCollider(Solid* owner, int _id, SDL_Rect col, bool istrigger)
{
  solid = owner;
  id = _id;
  collider = col;
  isTrigger = istrigger;
}

BoxCollider::~BoxCollider()
{
  if(box != nullptr)
  {
    RF_Engine::sendSignal(box, S_KILL);
  }
}

void BoxCollider::View()
{
  SDL_Rect n = solid->normalizeBound(id);

  if(box == nullptr && n.w > 1 && n.h > 1)
  {
    box = RF_Engine::getTask<RF_Process>(RF_Engine::newTask<RF_Process>(solid->id));
    box->graph = SDL_CreateRGBSurface(0, n.w, n.h, 32, 0xff000000, 0x00ff0000, 0x0000ff00, 0x000000ff);
    SDL_FillRect(box->graph, NULL, SDL_MapRGB(box->graph->format, 1+rand()%255, 1+rand()%255, 1+rand()%255));
  }

  if(box != nullptr)
  {
    box->transform.position.x = n.x + (n.w >> 1) + Camera::instance->transform.position.x;
    box->transform.position.y = n.y + (n.h >> 1) + Camera::instance->transform.position.y;
    box->zLayer = solid->zLayer + 1000 + id;
  }
}

void BoxCollider::Hide()
{
  if(box != nullptr)
  {
    RF_Engine::sendSignal(box, S_KILL);
    box = nullptr;
  }
}

string SolidCollisionCoroutine::instanceid = "";
void SolidCollisionCoroutine::Init()
{
  if(SolidCollisionCoroutine::instanceid == "")
  {
    SolidCollisionCoroutine::instanceid = RF_Engine::newTask<SolidCollisionCoroutine>();
  }
}

SolidCollisionCoroutine::~SolidCollisionCoroutine()
{
  SolidCollisionCoroutine::instanceid = "";
}

void SolidCollisionCoroutine::Update()
{
  int ic, jc;
  map<string, Solid*>::iterator i, j;
  bool collided;

  for(i = solidList.begin(); i != solidList.end(); ++i)
  {
    if(i->second != nullptr)
    {
      i->second->BeforeMove();
      i->second->Move(i->second->force);
    }
  }

  for(i = solidList.begin(); i != solidList.end(); ++i)
  {
    for(j = i; j != solidList.end(); ++j)
    {
      collided = false;
      if(i->first == j->first || i->second == nullptr || j->second == nullptr || i->second->signal != S_AWAKE || j->second->signal != S_AWAKE){ continue; }

      //Para cada collider suyo
      for(ic = 0; ic < i->second->boxColliderList.size(); ic++)
      {
        //Para cada collider mío
        for(jc = 0; jc < j->second->boxColliderList.size(); jc++)
        {
          //Si se chocan
          SDL_Rect a = i->second->normalizeBound(ic);
          SDL_Rect b = j->second->normalizeBound(jc);

          SDL_Rect collisionRect = RF_Collision::getIntersection(a, b);

          if(collisionRect.w == 0 && collisionRect.h == 0)
              continue;

          if(!i->second->boxColliderList[ic].isTrigger) { i->second->IsColliding(ic, &(j->second->boxColliderList[jc])); }
          else { i->second->IsTriggering(ic, &(j->second->boxColliderList[jc])); }
          if(!j->second->boxColliderList[jc].isTrigger) { j->second->IsColliding(jc, &(i->second->boxColliderList[ic])); }
          else { j->second->IsTriggering(jc, &(i->second->boxColliderList[ic])); }

          collided = true;
        }
      }

      if(!collided)
      {
        i->second->CheckCollisionExit(0, &(j->second->boxColliderList[0]));
        j->second->CheckCollisionExit(0, &(i->second->boxColliderList[0]));
      }
    }
  }

  for(i = solidList.begin(); i != solidList.end(); ++i)
  {
    if(i->second != nullptr)
    {
      i->second->AfterMove();
    }
  }
}

void SolidCollisionCoroutine::RegisterSolid(Solid* solid)
{
  if(!RF_Engine::existsTask(SolidCollisionCoroutine::instanceid)){SolidCollisionCoroutine::Init();}

  SolidCollisionCoroutine *s = RF_Engine::getTask<SolidCollisionCoroutine>(SolidCollisionCoroutine::instanceid);
  s->solidList[solid->id] = solid;
  s->solidCount++;
}

void SolidCollisionCoroutine::RemoveSolid(string solid)
{
  if(!RF_Engine::existsTask(SolidCollisionCoroutine::instanceid)){return;}

  SolidCollisionCoroutine *s = RF_Engine::getTask<SolidCollisionCoroutine>(SolidCollisionCoroutine::instanceid);
  s->solidList[solid] = nullptr;
  s->solidCount--;

  if(s->solidCount == 0)
  {
    s->solidList.clear();
  }
}

bool SolidCollisionCoroutine::IsRegistered(string solid)
{
  if(!RF_Engine::existsTask(SolidCollisionCoroutine::instanceid)){return false;}

  SolidCollisionCoroutine *s = RF_Engine::getTask<SolidCollisionCoroutine>(SolidCollisionCoroutine::instanceid);
  return (s->solidList[solid] != nullptr);
}

Solid::Solid(string name):Sprite(name)
{
  addCollider(0, 0, 1, 1, false);
  SolidCollisionCoroutine::Init();
}

Solid::~Solid()
{
  SolidCollisionCoroutine::RemoveSolid(id);
}

void Solid::Start()
{
  SolidCollisionCoroutine::RegisterSolid(this);
  Awake();
}

void Solid::Draw()
{
  Sprite::Draw();

  if(updateCollider)
  {
    setCollider(0, 0, -(graph->h>>1), graph->w, graph->h, boxColliderList[0].isTrigger);
  }

  if(viewCollisionBox)
  {
    for(int i = boxColliderList.size()-1; i >= 0; i--)
    {
      boxColliderList[i].View();
    }
  }
}

int Solid::addCollider(int x_offset, int y_offset, int width, int height, bool istrigger)
{
  boxColliderList.push_back(BoxCollider(this, boxColliderList.size(), {x_offset, y_offset, width, height}, istrigger));
  return boxColliderList.size() - 1;
}

void Solid::setCollider(int _id, int x_offset, int y_offset, int width, int height, bool istrigger)
{
  boxColliderList[_id].collider = {x_offset, y_offset, width, height};
  boxColliderList[_id].isTrigger = istrigger;
}

void Solid::setToTrigger(int _id, bool istrigger)
{
  boxColliderList[_id].isTrigger = istrigger;
}

SDL_Rect Solid::normalizeBound(int _id)
{
  SDL_Rect normalized;
  normalized.x = (Sint16)realPosition.x + (Sint16)offset.x + boxColliderList[_id].collider.x - (boxColliderList[_id].collider.w >> 1);
  normalized.y = (Sint16)realPosition.y + (Sint16)offset.y + boxColliderList[_id].collider.y - (boxColliderList[_id].collider.h >> 1);
  normalized.w = boxColliderList[_id].collider.w;
  normalized.h = boxColliderList[_id].collider.h;

  return normalized;
}

void Solid::CheckCollisionExit(int _id, BoxCollider* collider)
{
  //Miramos si estaba ya colisionando o no
    bool found = false;
    if(colliding.size() > 0)
    {
      auto m = colliding.find(collider->solid->id);
      found = (m != colliding.end());
    }

  //Si estaba colisionando
    if(found)
    {
      if(!colliding[collider->solid->id])
      {
        OnCollisionExit(_id, collider);
      }
      else
      {
        OnTriggerExit(_id, collider);
      }

      colliding.erase(collider->solid->id);
    }
}

bool Solid::WasColliding(int _id, BoxCollider* collider, bool isTrigger)
{
  //if(collider->solid->id == id){return true;}

  //Miramos si estaba ya colisionando o no
    bool found = false;
    if(colliding.size() > 0)
    {
      auto m = colliding.find(collider->solid->id);
      found = (m != colliding.end());
    }

  //Si no estaba colisionando
    if(!found)
    {
      std::pair<string,bool> p (collider->solid->id, isTrigger);
      colliding.insert(p);
    }

  return found;
}

void Solid::IsColliding(int _id, BoxCollider* collider)
{
  //Si no estaba colisionando
    if(!WasColliding(_id, collider, false))
    {
      OnCollisionEnter(_id, collider);
    }

  //Si es el collider base y choca contra un collider sólido
    if(_id == 0 && !collider->isTrigger)
    {
      force.x = -force.x;
      force.y = -force.y;
      Move(force);
      force.x = 0;
      force.y = 0;

      collider->solid->force.x = -collider->solid->force.x;
      collider->solid->force.y = -collider->solid->force.y;
      collider->solid->Move(collider->solid->force);
      collider->solid->force.x = 0;
      collider->solid->force.y = 0;
    }

  //Llamamos al evento
    OnCollision(_id, collider);
}

void Solid::IsTriggering(int _id, BoxCollider* collider)
{
  //Si no estaba colisionando
    if(!WasColliding(_id, collider, true))
    {
      OnTriggerEnter(_id, collider);
    }

  //Llamamos al evento
    OnTrigger(_id, collider);
}
